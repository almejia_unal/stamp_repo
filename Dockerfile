FROM amd64/ubuntu:14.04
MAINTAINER Alejandro Mejia 

RUN mv /etc/apt/sources.list /etc/apt/sources.list.old
RUN echo 'deb mirror://mirrors.ubuntu.com/mirrors.txt trusty main restricted universe multiverse' >> /etc/apt/sources.list
RUN echo 'deb mirror://mirrors.ubuntu.com/mirrors.txt trusty-updates main restricted universe multiverse' >> /etc/apt/sources.list
RUN echo 'deb mirror://mirrors.ubuntu.com/mirrors.txt trusty-backports main restricted universe multiverse' >> /etc/apt/sources.list
RUN echo 'deb mirror://mirrors.ubuntu.com/mirrors.txt trusty-security main restricted universe multiverse' >> /etc/apt/sources.list
RUN apt-get update -q 

#install software requirements
RUN  DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y software-properties-common build-essential git symlinks expect

# Install build dependancies
RUN apt-get purge binutils-arm-none-eabi \
gcc-arm-none-eabi \
gdb-arm-none-eabi \
libnewlib-arm-none-eabi

#install Debugging dependancies
#install OPENOCD Build dependancies and gdb
RUN apt-get update -q
RUN DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y \
libhidapi-hidraw0 \
libusb-0.1-4 \
libusb-1.0-0 \
libhidapi-dev \
libusb-1.0-0-dev \
libusb-dev \
libtool \
make \
automake \
pkg-config \
autoconf \
texinfo \
libx32ncurses5 \
libx32z1 \
wget \
libcurl4-openssl-dev \
rsync \
subversion \
bc \
unzip \
gcc-4.7 \
nano \
python2.7

EXPOSE 4444
