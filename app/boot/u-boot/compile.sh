#!/bin/sh
echo "Building U-Boot..."
make clean -j6
make mrproper -j6
make mx28evk_defconfig -j6
make CROSS_COMPILE="arm-none-eabi-" u-boot.sb -j6
echo "Converting .sb to .sd..."
./tools/mxsboot sd u-boot.sb u-boot.sd
echo "..."
echo "Done!"
