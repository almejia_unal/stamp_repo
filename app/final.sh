#!/bin/bash
wget https://launchpad.net/gcc-arm-embedded/4.9/4.9-2015-q3-update/+download/gcc-arm-none-eabi-4_9-2015q3-20150921-linux.tar.bz2
tar jxf gcc-arm-none-eabi-4_9-2015q3-20150921-linux.tar.bz2
git clone https://bitbucket.org/acuerol/stamp_repo.git
export PATH="/usr/src/app/gcc-arm-none-eabi-4_9-2015q3/bin/:$PATH"
ln -s /usr/bin/python2.7 /usr/bin/python
cd /usr/src/app/stamp_repo/files/boot/u-boot/
chmod +x compile.sh
apt update
apt install nano unzip bc subversion lib32ncurses5-dev -y
rm /usr/bin/gcc
apt install gcc-4.7 -y
ln -s /usr/bin/gcc-4.7 /usr/bin/gcc

